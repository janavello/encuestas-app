import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { HomeComponent } from './pages/home/home.component';
import { CrearencuestaComponent } from './pages/crearencuesta/crearencuesta.component';
import { CreardashboardComponent } from './pages/creardashboard/creardashboard.component';
import { VerdashboardComponent } from './pages/verdashboard/verdashboard.component';
import { VerencuestaComponent } from './pages/verencuesta/verencuesta.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    CrearencuestaComponent,
    CreardashboardComponent,
    VerdashboardComponent,
    VerencuestaComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
