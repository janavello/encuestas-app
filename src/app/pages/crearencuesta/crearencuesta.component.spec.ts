import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearencuestaComponent } from './crearencuesta.component';

describe('CrearencuestaComponent', () => {
  let component: CrearencuestaComponent;
  let fixture: ComponentFixture<CrearencuestaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrearencuestaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearencuestaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
