import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerencuestaComponent } from './verencuesta.component';

describe('VerencuestaComponent', () => {
  let component: VerencuestaComponent;
  let fixture: ComponentFixture<VerencuestaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerencuestaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerencuestaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
