import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreardashboardComponent } from './creardashboard.component';

describe('CreardashboardComponent', () => {
  let component: CreardashboardComponent;
  let fixture: ComponentFixture<CreardashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreardashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreardashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
