import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VerdashboardComponent } from './verdashboard.component';

describe('VerdashboardComponent', () => {
  let component: VerdashboardComponent;
  let fixture: ComponentFixture<VerdashboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VerdashboardComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerdashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
